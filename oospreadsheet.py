#!/usr/bin/python3
"""
	OOSpreadsheet - basic OpenOffice spreadsheet class for Python3
	
	@author: Andrei Matveyeu
	@date: 2012-10-08
	@contact: verkstad@ideabulbs.com
	@license: GPL3

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
"""
import zipfile
from xml.dom import minidom

class OOSpreadsheet(object):
	def __init__(self, filename):
		zf = zipfile.ZipFile(filename)
		self._root = minidom.parse(zf.open('content.xml'))
		
	def get_cells_from_sheet(self, sheet_name):
		"""Get a list of cell objects from the given sheet.
		"""
		sheets = self._root.getElementsByTagNameNS("*", "table")
		for sheet in sheets:
			if sheet.getAttribute('table:name')==sheet_name:
				print("found!")
				return sheet.getElementsByTagNameNS("*", "table-cell")
		return None
	
	def get_row_cells_from_sheet(self, sheet_name, row_number):
		"""Get a list of cells from the given sheet and row.
		"""
		sheets = self._root.getElementsByTagNameNS("*", "table")
		sheet = None
		for s in sheets:
			if s.getAttribute('table:name') == sheet_name:
				sheet = s
		if not sheet:
			return None
		rows = sheet.getElementsByTagNameNS("*", "table-row")
		return rows[row_number].getElementsByTagNameNS("*", "table-cell")

	def get_cell_from_sheet_by_xy(self, sheet_name, x, y):
		"""Get cell object from the given sheet by XY-coordinates"""
		row = self.get_row_cells_from_sheet(sheet_name, y)
		return row[x]

	def get_cell_text_from_sheet_by_xy(self, sheet_name, x, y):
		cell = self.get_cell_from_sheet_by_xy(sheet_name, x, y)
		return cell.firstChild.firstChild.data
		
	def get_cells(self):
		"""Get a list of all cells in the document.
		"""
		return self._root.getElementsByTagNameNS("*", "table-cell")
	
if __name__ == "__main__":
	s = OOSpreadsheet("finddata.ods")
	# get the text value from a specific cell/sheet
	print(s.get_cell_text_from_sheet_by_xy("Sheet1", 0, 0))
	
	# get a row by number from a specific sheet
	print(s.get_row_cells_from_sheet("Sheet1", 0))
	
	# get all cells from a specific sheet
	print(s.get_cells_from_sheet("Sheet1"))
	
	# get all cells from the spreadsheet
	print(s.get_cells()[0].toprettyxml())
